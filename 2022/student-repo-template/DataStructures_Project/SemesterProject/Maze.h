#ifndef _MAZE_H
#define _MAZE_H

#include <string>
#include <iostream>
#include <vector>
using namespace std;

struct Coordinate
{
	Coordinate() { }
	Coordinate(int x, int y) {
		this->x = x;
		this->y = y;
	}
	int x;
	int y;
};

class Maze
{
public:
	Maze();
	void Load(string filename);
	int GetWidth();
	int GetHeight();
	void Display(ostream& out);
	Coordinate GetStartLocation();
	Coordinate GetEndLocation();
	char GetTile(Coordinate coord);
	bool IsWalkable(Coordinate coord);
	void PlacePlayer(Coordinate coord);
	vector<Coordinate> GetNeighboringPaths(Coordinate coord);

private:
	const int WIDTH;
	const int HEIGHT;
	char m_mapGrid[10][10];
	Coordinate m_playerLocation;
	Coordinate m_startLocation;
	Coordinate m_endLocation;
};

#endif
