#include "Maze.h"

#include <iostream>
#include <fstream>
using namespace std;

Maze::Maze()
	: WIDTH( 10 ), HEIGHT( 10 )
{
    m_playerLocation.x = -1;
    m_playerLocation.y = -1;
}

void Maze::Load(string filename)
{
	ifstream input(filename);
	string line;
	int y = 0;
	while (getline(input, line))
	{
		for (int x = 0; x < WIDTH; x++)
		{
			m_mapGrid[x][y] = line[x];

			if (m_mapGrid[x][y] == 'S')
			{
				m_startLocation.x = x;
				m_startLocation.y = y;
			}
			else if (m_mapGrid[x][y] == 'E')
			{
				m_endLocation.x = x;
				m_endLocation.y = y;
			}
		}
		y++;
	}
}

int Maze::GetWidth()
{
	return WIDTH;
}

int Maze::GetHeight()
{
	return HEIGHT;
}

void Maze::Display(ostream& out)
{
	for (int y = 0; y < HEIGHT; y++)
	{
		for (int x = 0; x < WIDTH; x++)
		{
			out << m_mapGrid[x][y];
		}
		out << endl;
	}
}

Coordinate Maze::GetStartLocation()
{
	return m_startLocation;
}

Coordinate Maze::GetEndLocation()
{
	return m_endLocation;
}

char Maze::GetTile(Coordinate coord)
{
	if (coord.x >= 0 && coord.x < WIDTH && coord.y >= 0 && coord.y <= HEIGHT)
	{
		return m_mapGrid[coord.x][coord.y];
	}
	return 'x';
}

bool Maze::IsWalkable(Coordinate coord)
{
	if (coord.x >= 0 && coord.x < WIDTH && coord.y >= 0 && coord.y <= HEIGHT)
	{
		return (
			m_mapGrid[coord.x][coord.y] == ' ' ||
			m_mapGrid[coord.x][coord.y] == 'S' ||
			m_mapGrid[coord.x][coord.y] == 'E');
	}
	return false;
}

void Maze::PlacePlayer(Coordinate coord)
{
    // Visited
    if ( m_playerLocation.x != -1 )
    {
        m_mapGrid[m_playerLocation.x][m_playerLocation.y] = '-';
    }
	m_mapGrid[coord.x][coord.y] = '@';
	m_playerLocation = coord;
}

vector<Coordinate> Maze::GetNeighboringPaths(Coordinate coord)
{
	vector<Coordinate> validNeighbors;

	Coordinate neighbors[4] = {
		Coordinate(coord.x, coord.y - 1), // north
		Coordinate(coord.x, coord.y + 1), // south
		Coordinate(coord.x - 1, coord.y), // west
		Coordinate(coord.x + 1, coord.y) // east
	};

	for (int i = 0; i < 4; i++)
	{
		if (neighbors[i].x >= 0 && neighbors[i].x < WIDTH && neighbors[i].y >= 0 && neighbors[i].y <= HEIGHT && (
			m_mapGrid[neighbors[i].x][neighbors[i].y] == ' ' ||
			m_mapGrid[neighbors[i].x][neighbors[i].y] == 'S' ||
			m_mapGrid[neighbors[i].x][neighbors[i].y] == 'E'
			))
		{
			// VALID
			validNeighbors.push_back(neighbors[i]);
		}
	}

	return validNeighbors;
}
