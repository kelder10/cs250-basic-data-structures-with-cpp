#ifndef _STACK_TESTER_HPP
#define _STACK_TESTER_HPP

// C++ Library includes
#include <iostream>
#include <string>

#include "ArrayStack.h"
#include "LinkedStack.h"
#include "../../cutest/TesterBase.h"
#include "../../Utilities/Menu.h"
#include "../../Utilities/StringUtil.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

//! TESTER for the Stack
class StackTester : public cuTest::TesterBase
{
public:
    StackTester()
        : TesterBase( "test_result_stack.html" )
    {
        AddTest(cuTest::TestListItem("Test_ArrayStack",              std::bind(&StackTester::Test_ArrayStack, this)));
        AddTest(cuTest::TestListItem("Test_LinkedStack",             std::bind(&StackTester::Test_LinkedStack, this)));
    }

    virtual ~StackTester() { }

private:
    int Test_ArrayStack();
    int Test_LinkedStack();
};

int StackTester::Test_ArrayStack()
{
    StartTestSet( "StackTester::Test_ArrayStack", { "Push", "Get", "Pop", "Size", "IsEmpty" } );
    Utility::Logger::Out( "Beginning test", "StackTester::ArrayStack" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "ArrayStack - Check if Push, Get, Pop, Size, and IsEmpty is implemented" );
        try
        {
            ArrayStack<std::string> q;
            q.Push( "hi" );
            q.Top();
            q.Pop();
            q.Size();
            q.IsEmpty();
        }
        catch( std::runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
            Utility::Logger::Out( "EX: " + std::string(ex.what()) + " -- ArrayStack not fully implemented. Skipping the rest of this test set.", "VectorTester::Test_LinkedStack" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayStack - Empty Stack, check if IsEmpty returns true." );

        ArrayStack<std::string> q;

        bool expectedResult = true;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayStack - Add to Stack, check if IsEmpty is false" );

        ArrayStack<std::string> q;
        q.Push( "A" );

        bool expectedResult = false;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayStack - Push items, check Size." );

        ArrayStack<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        int expectedResult = 3;
        int actualResult = q.Size();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayStack - Push items, use Get and Pop to validate." );

        ArrayStack<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        std::string expectedResult[] = { "C", "B", "A" };

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            std::string frontValue = q.Top();
            q.Pop();

            Set_ExpectedOutput  ( "Element " + Utility::StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + Utility::StringUtil::ToString( i ), frontValue );

            if ( frontValue != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )    { TestFail(); }
        else                { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int StackTester::Test_LinkedStack()
{
    StartTestSet( "StackTester::Test_LinkedStack", { "Push", "Get", "Pop", "Size", "IsEmpty" } );
    Utility::Logger::Out( "Beginning test", "StackTester::LinkedStack" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "LinkedStack - Check if Push, Get, Pop, Size, and IsEmpty is implemented" );
        try
        {
            LinkedStack<std::string> q;
            q.Push( "hi" );
            q.Top();
            q.Pop();
            q.Size();
            q.IsEmpty();
        }
        catch( std::runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
            Utility::Logger::Out( "EX: " + std::string(ex.what()) + " -- LinkedStack not fully implemented. Skipping the rest of this test set.", "VectorTester::Test_LinkedStack" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedStack - Empty Stack, check if IsEmpty returns true." );

        LinkedStack<std::string> q;

        bool expectedResult = true;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedStack - Add to Stack, check if IsEmpty is false" );

        LinkedStack<std::string> q;
        q.Push( "A" );

        bool expectedResult = false;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedStack - Push items, check Size." );

        LinkedStack<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        int expectedResult = 3;
        int actualResult = q.Size();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedStack - Push items, use Get and Pop to validate." );

        LinkedStack<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        std::string expectedResult[] = { "C", "B", "A" };

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            std::string frontValue = q.Top();
            q.Pop();

            Set_ExpectedOutput  ( "Element " + Utility::StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + Utility::StringUtil::ToString( i ), frontValue );

            if ( frontValue != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )    { TestFail(); }
        else                { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

} // End of namespace

#endif
