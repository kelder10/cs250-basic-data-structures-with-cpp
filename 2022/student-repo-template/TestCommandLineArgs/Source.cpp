#include <iostream>
using namespace std;

// VS: Project Properties > Debugging > Command arguments
// CB: Project > Set programs' arguments
int main(int argc, char * argv[])
{
	cout << "Arg count: " << argc << endl;

	cout << "Arguments: " << endl;
	for (int i = 0; i < argc; i++)
	{
		cout << i << "\t" << argv[i] << endl;
	}

	return 0;
}
