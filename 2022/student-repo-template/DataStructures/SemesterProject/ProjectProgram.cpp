#include "ProjectProgram.h"
#include "Maze.h"
#include "../Utilities/Timer.h"
#include "../Utilities/StringUtil.h"
#include "../DataStructure/BinarySearchTree/BinarySearchTree.h"
#include "../DataStructure/Queue/LinkedQueue.h"
#include "../DataStructure/Queue/LinkedQueue.h"
#include "../DataStructure/SmartDynamicArray/SmartDynamicArray.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <stack>
#include <queue>
#include <vector>
#include <list>
using namespace std;

void ProjectProgram::Run()
{
	bool done = false;
	while (!done)
	{
		cout << endl << endl << "MAIN MENU" << endl;
		cout << string(80, '-') << endl;
		cout << "0. EXIT" << endl;
		cout << "1. Queue program" << endl;
		cout << "2. Compare - Binary Search Trees, Arrays, Linked Lists" << endl;
		cout << "3. Recursion program" << endl;

		cout << endl << ">> ";
		int choice;
		cin >> choice;

		switch (choice)
		{
		case 0: done = true; break;
		case 1: QueueProgram(); break;
		case 2: DataProcessing(); break;
		case 3: RecursionProgram(); break;
		}
	}
}

void ProjectProgram::QueueProgram()
{
    // TODO: Implement me!
}

void ProjectProgram::RecursionProgram()
{
    // TODO: Implement me!
}

bool ProjectProgram::MazeTraverse_Recursive(Maze& maze, Coordinate position, vector<Coordinate>& locationsVisited, int& step, ofstream& logFile)
{
    // TODO: Implement me!
}

void ProjectProgram::DataProcessing()
{
    // TODO: Implement me!
}

void ProjectProgram::DataProcessing_DynamicArray( ofstream& logFile, string inputFilePath )
{
	Timer timer;                        // Create a timer to time the push and get processes.
	string buffer;                      // Buffer to store file contents
	ifstream input( inputFilePath );    // Input file to read data from
	int counter = 0;                    // Counter (just for cout reasons)

	// The data structure itself:
	DataStructure::SmartDynamicArray<string> arrayData;

	// DYNAMIC ARRAY
	// Make a note in the output file:
	logFile << endl << endl << "Storing data from file into array... ";
	if ( input.fail() )
	{
        logFile << "ERROR OPENING FILE \"" << inputFilePath << "\"" << endl;
        return;
	}

	timer.Start();          // Begin the timer
	// Read each line of the file and store it
	while (getline(input, buffer))
	{
        counter++;
        if ( counter % 100000 )
        {
            // Display a message every few items so we know the program didn't crash,
            // and to give you something to look at while it runs :P
            cout << "* " << counter << " items loaded (Dynamic Array)" << endl;
        }
        // Push this data back in the array.
		arrayData.PushBack(buffer);
	}
	input.close();

	// Display how long all the Push functionality took
	logFile << timer.GetElapsedMilliseconds() << " milliseconds" << endl;

	// ACCESS TIME
	// Now we're timing how long to access some random items in the array:
	logFile << "Access 10 random items:" << endl;
	for (int i = 0; i < 10; i++)
	{
		int index = rand() % arrayData.Size();  // Get a random index

		logFile << "* Get item at index " << index << "... ";
		cout << "* Dynamic Array ------ Get item at index " << index << "... ";

		// Begin timer
		timer.Start();
		// Get the data
		string data = arrayData.GetAt(index);
		// Display the time elapsed
		logFile << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
	}
}

void ProjectProgram::DataProcessing_LinkedList( ofstream& logFile, string inputFilePath )
{
    // TODO: Implement me!
}

void ProjectProgram::DataProcessing_BinarySearchTree( ofstream& logFile, string inputFilePath )
{
    // TODO: Implement me!
}

