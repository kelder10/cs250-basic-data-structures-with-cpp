#ifndef _PROGRAM_H
#define _PROGRAM_H

#include "Maze.h"

class ProjectProgram
{
public:
	void Run();
	void QueueProgram();
	void RecursionProgram();
	void DataProcessing();
	void DataProcessing_DynamicArray( ofstream& logFile, string inputFilePath );
	void DataProcessing_LinkedList( ofstream& logFile, string inputFilePath );
	void DataProcessing_BinarySearchTree( ofstream& logFile, string inputFilePath );

private:
	bool MazeTraverse_Recursive(Maze& maze, Coordinate position, vector<Coordinate>& locationsVisited, int& step, ofstream& result);
};

#endif
