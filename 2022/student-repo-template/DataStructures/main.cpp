#include "Utilities/Logger.h"
#include "DataStructuresProgram.h"

int main()
{
    // Initialize the debug logger
    Utility::Logger::Setup();

    // Start the program
    DataStructuresProgram program;

    // In Windows, I think this might work for auto-open. :P
    program.SetWebBrowser( "explorer" ); 
    program.SetAutoOpenTestResults(true);
    program.MainMenu();

    // Clean up the debug logger
    Utility::Logger::Cleanup();

    return 0;
}







