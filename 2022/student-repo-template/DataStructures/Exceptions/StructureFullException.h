#ifndef STRUCTURE_FULL_EXCEPTION
#define STRUCTURE_FULL_EXCEPTION

// C++ Library includes
#include <stdexcept>
#include <string>

namespace Exception
{

//! EXCEPTION for when a structure is full
class StructureFullException : public std::runtime_error
{
    public:
    StructureFullException( std::string functionName, std::string message )
        : std::runtime_error( "[" + functionName + "] " + message  ) { ; }
};

} // End of namespace

#endif
