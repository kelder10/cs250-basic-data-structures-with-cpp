#ifndef _QUEUE_TESTER_HPP
#define _QUEUE_TESTER_HPP

// C++ Library includes
#include <iostream>
#include <string>

// Project includes
#include "ArrayQueue.h"
#include "LinkedQueue.h"
#include "../../cutest/TesterBase.h"
#include "../../Utilities/Menu.h"
#include "../../Utilities/StringUtil.h"
#include "../../Exceptions/NotImplementedException.h"
#include "../../Exceptions/InvalidIndexException.h"
#include "../../Exceptions/NullptrException.h"

namespace DataStructure
{

//! TESTER for the Queue
class QueueTester : public cuTest::TesterBase
{
public:
    QueueTester()
        : TesterBase( "test_result_queue.html" )
    {
        AddTest(cuTest::TestListItem("Test_ArrayQueue",              std::bind(&QueueTester::Test_ArrayQueue, this)));
        AddTest(cuTest::TestListItem("Test_LinkedQueue",             std::bind(&QueueTester::Test_LinkedQueue, this)));
    }

    virtual ~QueueTester() { }

private:
    int Test_ArrayQueue();
    int Test_LinkedQueue();
};

int QueueTester::Test_ArrayQueue()
{
    StartTestSet( "QueueTester::Test_ArrayQueue", { "Push", "Get", "Pop", "Size", "IsEmpty" } );
    Utility::Logger::Out( "Beginning test", "QueueTester::ArrayQueue" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "ArrayQueue - Check if Push, Get, Pop, Size, and IsEmpty is implemented" );
        try
        {
            ArrayQueue<std::string> q;
            q.Push( "hi" );
            q.Front();
            q.Pop();
            q.Size();
            q.IsEmpty();
        }
        catch( std::runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
            Utility::Logger::Out( "EX: " + std::string(ex.what()) + " -- ArrayQueue not fully implemented. Skipping the rest of this test set.", "VectorTester::Test_LinkedQueue" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayQueue - Empty queue, check if IsEmpty returns true." );

        ArrayQueue<std::string> q;

        bool expectedResult = true;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayQueue - Add to queue, check if IsEmpty is false" );

        ArrayQueue<std::string> q;
        q.Push( "A" );

        bool expectedResult = false;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayQueue - Push items, check Size." );

        ArrayQueue<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        int expectedResult = 3;
        int actualResult = q.Size();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "ArrayQueue - Push items, use Get and Pop to validate." );

        ArrayQueue<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        std::string expectedResult[] = { "A", "B", "C" };

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            std::string frontValue = q.Front();
            q.Pop();

            Set_ExpectedOutput  ( "Element " + Utility::StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + Utility::StringUtil::ToString( i ), frontValue );

            if ( frontValue != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )    { TestFail(); }
        else                { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

int QueueTester::Test_LinkedQueue()
{
    StartTestSet( "QueueTester::Test_LinkedQueue", { "Push", "Get", "Pop", "Size", "IsEmpty" } );
    Utility::Logger::Out( "Beginning test", "QueueTester::LinkedQueue" );

    bool exceptOccur = false;
    {
        /* TEST BEGIN ************************************************************/
        StartTest( "LinkedQueue - Check if Push, Get, Pop, Size, and IsEmpty is implemented" );
        try
        {
            LinkedQueue<std::string> q;
            q.Push( "hi" );
            q.Front();
            q.Pop();
            q.Size();
            q.IsEmpty();
        }
        catch( const std::runtime_error& ex )
        {
            exceptOccur = true;
            TestFail();
            Utility::Logger::Out( "EX: " + std::string( ex.what() ) + " -- LinkedQueue not fully implemented. Skipping the rest of this test set.", "VectorTester::Test_LinkedQueue" );
        }

        if ( !exceptOccur )
        {
            TestPass();
        }
        FinishTest();
    } /* TEST END **************************************************************/

    if ( exceptOccur )
    {
        FinishTestSet();
        return TestResult();
    }

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedQueue - Empty queue, check if IsEmpty returns true." );

        LinkedQueue<std::string> q;

        bool expectedResult = true;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedQueue - Add to queue, check if IsEmpty is false" );

        LinkedQueue<std::string> q;
        q.Push( "A" );

        bool expectedResult = false;
        bool actualResult = q.IsEmpty();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedQueue - Push items, check Size." );

        LinkedQueue<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        int expectedResult = 3;
        int actualResult = q.Size();

        if ( actualResult != expectedResult )   { TestFail(); }
        else                                    { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    { /* TEST BEGIN ************************************************************/
        StartTest( "LinkedQueue - Push items, use Get and Pop to validate." );

        LinkedQueue<std::string> q;
        q.Push( "A" );
        q.Push( "B" );
        q.Push( "C" );

        std::string expectedResult[] = { "A", "B", "C" };

        bool allMatch = true;
        for ( int i = 0; i < 3; i++ )
        {
            std::string frontValue = q.Front();
            q.Pop();

            Set_ExpectedOutput  ( "Element " + Utility::StringUtil::ToString( i ), expectedResult[i] );
            Set_ActualOutput    ( "Element " + Utility::StringUtil::ToString( i ), frontValue );

            if ( frontValue != expectedResult[i] )
            {
                allMatch = false;
            }
        }

        if ( !allMatch )    { TestFail(); }
        else                { TestPass(); }

        FinishTest();
    } /* TEST END **************************************************************/

    FinishTestSet();
    return TestResult();
}

} // End of namespace

#endif
