#include "Folder.hpp"

Folder::Folder() {}
Folder::Folder( string name )
    : FileSystemThing( name, "Folder" )
{
}

void Folder::Display()
{
    FileSystemThing::Display();
    cout << "/";
}

void Folder::Details()
{
    FileSystemThing::Details();
    cout << "Total subfolders:  " << m_childFolders.size() << endl;
    cout << "Total files:       " << m_childFiles.size() << endl;
    cout << endl;
}

void Folder::AddFile( File* ptrFile )
{
    m_childFiles.push_back( ptrFile );
}

void Folder::AddFolder( Folder* ptrFolder )
{
    m_childFolders.push_back( ptrFolder );
}

vector<Folder*>& Folder::GetFolders()
{
    return m_childFolders;
}

vector<File*>& Folder::GetFiles()
{
    return m_childFiles;
}
