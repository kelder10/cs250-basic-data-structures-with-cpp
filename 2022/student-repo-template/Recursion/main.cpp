#include <iostream>
using namespace std;

#include "FileSystemManager.hpp"
#include "utilities/Menu.hpp"
#include "utilities/Logger.hpp"

int main()
{
    Logger::Setup();
    bool done = false;
    FileSystemManager filesystem;

    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "MAIN MENU" );

        string choice = Menu::ShowStringMenuWithPrompt( {
            "Search for file",
            "Search for folder",
            "View filesystem",
            "Quit"
        } );

        if ( choice == "Search for file" )
        {
            string name = Menu::GetStringChoice( "Enter file name:" );
            File* ptrFile = filesystem.FindFile( name );

            cout << "Address:           " << ptrFile << endl;
            if ( ptrFile != nullptr )
            {
                ptrFile->Details();
            }
        }
        else if ( choice == "Search for folder" )
        {
            string name = Menu::GetStringChoice( "Enter folder name:" );
            Folder* ptrFolder = filesystem.FindFolder( name );

            cout << "Address:           " << ptrFolder << endl;
            if ( ptrFolder != nullptr )
            {
                ptrFolder->Details();
            }
        }
        else if ( choice == "View filesystem" )
        {
            filesystem.Display();
        }
        else if ( choice == "Quit" )
        {
            done = true;
        }

        Menu::Pause();
    }

    Logger::Cleanup();
    return 0;
}
