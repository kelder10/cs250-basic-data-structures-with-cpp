#ifndef _FILE
#define _FILE

#include "FileSystemThing.hpp"

class File : public FileSystemThing
{
    public:
    File();
    File( string name, string contents );

    void Setup( string contents );
    void Display();
    void Details();

    protected:
    int     m_bytes;
    string  m_contents;

    friend class FileSystemManager;
};

#endif
