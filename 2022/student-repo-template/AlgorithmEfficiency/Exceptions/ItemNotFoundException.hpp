#ifndef _ITEM_NOT_FOUND_EXCEPTION
#define _ITEM_NOT_FOUND_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

//! EXCEPTION for item not found in a structure
class ItemNotFoundException : public runtime_error
{
    public:
    ItemNotFoundException( string functionName, string message )
        : runtime_error( "[" + functionName + "] " + message  ) { ; }
};

#endif
