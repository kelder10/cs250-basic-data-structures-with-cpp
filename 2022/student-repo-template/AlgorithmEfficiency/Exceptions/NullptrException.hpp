#ifndef _NULLPTR_EXCEPTION
#define _NULLPTR_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

//! EXCEPTION for when a nullptr is detected when it is not expected
class NullptrException : public runtime_error
{
    public:
    NullptrException( string functionName, string message )
        : runtime_error( "[" + functionName + "] " + message ) { ; }
};

#endif
