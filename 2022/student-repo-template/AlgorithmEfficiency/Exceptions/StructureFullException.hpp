#ifndef STRUCTURE_FULL_EXCEPTION
#define STRUCTURE_FULL_EXCEPTION

#include <stdexcept>
#include <string>
using namespace std;

//! EXCEPTION for when a structure is full
class StructureFullException : public runtime_error
{
    public:
    StructureFullException( string functionName, string message )
        : runtime_error( "[" + functionName + "] " + message  ) { ; }
};

#endif
