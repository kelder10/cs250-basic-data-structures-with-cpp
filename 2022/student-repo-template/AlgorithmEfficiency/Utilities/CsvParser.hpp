#ifndef _CSV_PARSER_HPP
#define _CSV_PARSER_HPP

#include "StringUtil.hpp"

#include <string>
#include <fstream>
#include <vector>
using namespace std;

//! A simple structure to represent CSV documents
struct CsvDocument
{
    vector<string> header;
    vector< vector< string > > rows;
};

//! A simple class to parse CSV files
class CsvParser
{
    public:
    static CsvDocument Parse( string filepath );
    static void Save( string filepath, const CsvDocument& doc );
    static vector<string> CsvSplit( string str, char delim );
};

#endif
