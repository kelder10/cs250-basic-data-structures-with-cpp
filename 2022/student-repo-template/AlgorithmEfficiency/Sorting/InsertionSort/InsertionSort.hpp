#ifndef _INSERTION_SORT_HPP
#define _INSERTION_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace InsertionSort
{

template <typename T>
void Sort( vector<T>& arr )
{
    size_t arraySize = arr.size();
    size_t i = 1;

    while ( i < arraySize )
    {
        int j = i;
        while ( j > 0 && arr[j-1] > arr[j] )
        {
            swap( arr[j], arr[j-1] );
            j = j - 1;
        }

        i = i + 1;
    }
}

}

#endif
