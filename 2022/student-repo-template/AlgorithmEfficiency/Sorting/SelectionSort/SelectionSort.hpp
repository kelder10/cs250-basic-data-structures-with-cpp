#ifndef _SELECTION_SORT_HPP
#define _SELECTION_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace SelectionSort
{

template <typename T>
void Sort( vector<T>& arr )
{
    int arraySize = arr.size();

    for ( size_t i = 0; i < arraySize - 1; i++ )
    {
        int minIndex = i;

        for ( size_t j = i + 1; j < arraySize; j++ )
        {
            if ( arr[j] < arr[minIndex] )
            {
                minIndex = j;
            }
        }

        if ( minIndex != i )
        {
            swap( arr[i], arr[minIndex] );
        }
    }
}

}

#endif
