#ifndef _BUBBLE_SORT_HPP
#define _BUBBLE_SORT_HPP

#include "../../Exceptions/NotImplementedException.hpp"

#include <algorithm>    // includes swap()
using namespace std;

namespace BubbleSort
{

template <typename T>
void Sort( vector<T>& arr )
{
    for ( int i = 0; i < arr.size() - 1; i++ )
    {
        for ( int j = 0; j < arr.size() - i - 1; j++ )
        {
            if ( arr[j] > arr[j+1] )
            {
                swap( arr[j], arr[j+1] );
            }
        }
    }
}

}

#endif
