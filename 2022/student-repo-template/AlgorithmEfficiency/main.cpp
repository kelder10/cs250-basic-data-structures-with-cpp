#include <iostream>
using namespace std;

#include "CollegeCovidProgram.hpp"
#include "VetProgram.hpp"

int main()
{
    CollegeCovidProgram ccprogram;
    VetProgram vprogram;

    bool done = false;
    while ( !done )
    {
        cout << endl << endl << string( 80, '-' ) << endl;
        cout << "- MAIN MENU -" << endl;
        cout << "1. College Covid Program" << endl;
        cout << "2. Vet Program" << endl;
        cout << "3. Quit" << endl;

        int choice;
        cin >> choice;

        switch( choice )
        {
            case 1:
            ccprogram.Run( "data/" );
            break;

            case 2:
            vprogram.Run( "data/" );
            break;

            case 3:
            done = true;
            break;
        }
    }

    return 0;
}
