#ifndef _ANIMAL_INTAKE_HPP
#define _ANIMAL_INTAKE_HPP

#include <vector>
#include <string>
using namespace std;

// Structure in the same format as the data from the csv file
// Shelter,Animal ID#,Intake Date,Intake Type,Intake Condition,Animal Type,Group,Breed 1,Breed 2
struct AnimalIntake
{
    string Shelter;
    string AnimalID;
    string IntakeDate;
    string IntakeType;
    string IntakeCondition;
    string AnimalType;
    string Group;
    string Breed1;
    string Breed2;

    void Set( const vector<string>& header, const vector<string>& row );

    friend bool operator==( const AnimalIntake& left, const AnimalIntake& right );
    friend bool operator!=( const AnimalIntake& left, const AnimalIntake& right );
    friend bool operator<( const AnimalIntake& left, const AnimalIntake& right );
    friend bool operator>( const AnimalIntake& left, const AnimalIntake& right );
    friend bool operator<=( const AnimalIntake& left, const AnimalIntake& right );
    friend bool operator>=( const AnimalIntake& left, const AnimalIntake& right );

    friend ostream& operator<<( ostream& out, const AnimalIntake& item );
};

#endif
