#include "AnimalIntake.hpp"

#include "../Utilities/StringUtil.hpp"

#include <iostream>
#include <iomanip>
using namespace std;

//    string Shelter;
//    string AnimalID;
//    string IntakeDate;
//    string IntakeType;
//    string IntakeCondition;
//    string AnimalType;
//    string Group;
//    string Breed1;
//    string Breed2;

void AnimalIntake::Set( const vector<string>& header, const vector<string>& row )
{
    for ( unsigned int i = 0; i < row.size(); i++ )
    {
        if      ( header[i] == "Shelter" )              { Shelter = row[i]; }
        else if ( header[i] == "Animal ID#" )           { AnimalID = row[i]; }
        else if ( header[i] == "Intake Date" )          { IntakeDate = row[i]; }
        else if ( header[i] == "Intake Type" )          { IntakeType = row[i]; }
        else if ( header[i] == "Intake Condition" )     { IntakeCondition = row[i]; }
        else if ( header[i] == "Animal Type" )          { AnimalType = row[i]; }
        else if ( header[i] == "Group" )                { Group = row[i]; }
        else if ( header[i] == "Breed 1" )              { Breed1 = row[i]; }
        else if ( header[i] == "Breed 2" )              { Breed2 = row[i]; }
    }
}

bool operator==( const AnimalIntake& left, const AnimalIntake& right )
{
    return ( left.AnimalID == right.AnimalID );
}

bool operator!=( const AnimalIntake& left, const AnimalIntake& right )
{
    return !( left == right );
}

bool operator<( const AnimalIntake& left, const AnimalIntake& right )
{
    return ( left.AnimalID < right.AnimalID );
}

bool operator>( const AnimalIntake& left, const AnimalIntake& right )
{
    return ( left.AnimalID > right.AnimalID );
}

bool operator<=( const AnimalIntake& left, const AnimalIntake& right )
{
    return ( left < right || left == right );
}

bool operator>=( const AnimalIntake& left, const AnimalIntake& right )
{
    return ( left > right || left == right );
}
//    string Shelter;
//    string AnimalID;
//    string IntakeDate;
//    string IntakeType;
//    string IntakeCondition;
//    string AnimalType;
//    string Group;
//    string Breed1;
//    string Breed2;
ostream& operator<<( ostream& out, const AnimalIntake& item )
{
    const int W = 80/5;
    out << item.AnimalID << endl
        << left
        << setw( 10 ) << "* SHELTER:" << setw( W ) << item.Shelter
        << setw( 10 ) << "* DATE:" << setw( W ) << item.IntakeDate
        << setw( 10 ) << "* TYPE:" << setw( W ) << item.IntakeType
        << setw( 10 ) << "* ANIMAL:" << setw( W ) << item.AnimalType
        << setw( 10 ) << "* BREED:" << setw( W/2 ) << item.Breed1 << setw( W/2 ) << item.Breed2
        << endl;
    return out;
}
