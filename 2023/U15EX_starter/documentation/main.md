# Getting started

Go to the nav menu at the top and select **Classes** > **Class List**
and then the data structure you're working on (e.g., **SmartFixedArray**)
to view its documentation.
